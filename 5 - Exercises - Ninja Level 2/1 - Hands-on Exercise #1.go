package main

import "fmt"

func main() {
	x := 42
	fmt.Printf("%d\t%b\t%#X\t%x", x, x, x, x)
}
