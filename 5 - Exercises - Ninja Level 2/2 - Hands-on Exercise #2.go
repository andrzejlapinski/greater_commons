package main

import "fmt"

func main() {
	a := 1==1
	b := 1 <= 0
	c := 1 >= 0
	d := 1 != 1
	e := 1 < 0
	f := 1 > 0

	fmt.Println(a, b, c, d, e, f)
}
