package main

import "fmt"

const (
	a = 42 // untyped constant
	b int = 32

)

func main() {
	fmt.Println(a, b)
}
