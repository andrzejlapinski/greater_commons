package main

import "fmt"

func main() {
	x := 42
	y := 42.3454
	fmt.Printf("%v - %T\n", x, x)
	fmt.Printf("%v - %T\n", y, y)

	// int8 - również wartości ujemne (-128, 127)
	// uint8 - same wartości dodatnie (0, 255)
}
