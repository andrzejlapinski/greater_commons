package main

import "fmt"

//const a = 42
//const b = 42.24

const (
	a = 42
	b = 42.24
)


func main() {
	fmt.Printf("%T\n", a)
	fmt.Printf("%T\n", b)
}
