package main

import "fmt"

type tstType int
var x tstType
func main() {
	fmt.Println(x)
	fmt.Printf("%T\n", x)
	x = 42
	fmt.Println(x)
}
