package main

import "fmt"

var r int
type hotdog int
var b hotdog

func main() {
	r = 42
	b = 44
	fmt.Println(r)
	fmt.Printf("%T\n", r)
	fmt.Println(b)
	fmt.Printf("%T\n", b)
}
