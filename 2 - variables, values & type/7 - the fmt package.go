package main

import "fmt"

var a int
var b string = "test"
var c bool
var d bool = true

func main() {
	e := 42
	f := "test 2"
	g := `tmp raw string, "TEST RAW"`
	h := `another raw
	raw
	raw
	`
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
	fmt.Println(d)
	fmt.Println(e)
	fmt.Println(f)
	fmt.Println(g)
	fmt.Println(h)

	fmt.Printf("%v %v \n\t\t%v\a\n", a, b, d)
	fmt.Printf("%#v %#v \n\t\t%#v\a\n", a, b, d)
	fmt.Printf("%T %T \n\t\t%v\a\n", a, b, d)

	s := fmt.Sprint(a, b)
	s1 := fmt.Sprintf("%v %d %T", a, a, a)
	fmt.Println(s)
	fmt.Println(s1)
}
