package main

import "fmt"

var z = 21

func main() {
	fmt.Println(z)
	foo()
	fmt.Println(z)
}

func foo() {
	z += 1
	fmt.Println(z)
}