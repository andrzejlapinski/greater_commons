package main

import "fmt"

func main() {
	x := 42
	y := "test"
	fmt.Println(x, y)
	x = 50
	fmt.Println(x)
}
